package com.example.crudservice.crudservice;

import static com.mongodb.client.model.Projections.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.example.crudservice.crudservice.service.CrudService;
import com.example.crudservice.crudservice.service.MongoService;

import org.bson.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@SuppressWarnings("MagicConstant")
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CrudServiceTest {

     @MockBean
    private MongoService mongoService;

    @Autowired
    private CrudService crudService;

   



   @Test
   public void readDatatestMock()
   {
        String isi = "{\"nama\":\"ulwicuk\",\"alamat\":\"rancaekekbergoyang\",\"pekerjaan\":\"ggwp\"}";
        List<Document> value = new ArrayList<>();
        value.add(Document.parse(isi));
        when(mongoService.getDataCrud()).thenReturn(value);

        List<Document> result = this.crudService.readData();
        
     assertEquals(value, result.get(0).get("content"));
   }

   @Test
   public void createDataTestMock(){
     String isi = "{\"nama\":\"ulwicuk\",\"alamat\":\"rancaekekbergoyang\",\"pekerjaan\":\"ggwp\"}";
     when(mongoService.insertDocument(anyObject())).thenReturn("inserted");

     String result = this.crudService.create(isi);

     assertEquals("inserted", result);
   }

   @Test 
   public void updateDataTestMock(){
     String isi = "{\"nama\":\"ulwi\",\"alamat\":\"rancaekekbergoyang\",\"pekerjaan\":\"ggwp\"}";
     when(mongoService.updateDocument(anyString(), anyObject())).thenReturn("updated");

     String result = this.crudService.update("ulwicuk", isi);

     assertEquals("updated", result);

   }

   @Test
   public void delteDataTestMock(){
        when(mongoService.deleteDocument(anyString())).thenReturn("deleted");
        String result = this.crudService.delete("ulwicuk");
        assertEquals("deleted", result);
   }

}
// package com.example.crudservice.crudservice;

// import java.io.File;
// import java.util.ArrayList;
// import java.util.List;
// import java.util.Scanner;

// import com.example.crudservice.crudservice.service.MongoService;
// import com.mongodb.MongoClient;
// import com.mongodb.client.MongoCollection;
// import com.mongodb.client.MongoDatabase;

// import org.bson.Document;
// import org.bson.conversions.Bson;
// import org.junit.BeforeClass;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.test.context.ActiveProfiles;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.context.junit4.SpringRunner;

// import de.flapdoodle.embed.mongo.MongodExecutable;
// import de.flapdoodle.embed.mongo.MongodProcess;
// import de.flapdoodle.embed.mongo.MongodStarter;
// import de.flapdoodle.embed.mongo.config.IMongoConfig;
// import de.flapdoodle.embed.mongo.config.IMongodConfig;
// import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
// import de.flapdoodle.embed.mongo.config.Net;
// import de.flapdoodle.embed.mongo.distribution.Version;
// import de.flapdoodle.embed.process.runtime.Network;
// import com.mongodb.client.model.Projections;

// import static com.mongodb.client.model.Projections.*;
// import static org.junit.Assert.assertEquals;
// @SuppressWarnings("MagicConstant")
// @ActiveProfiles("test")
// @RunWith(SpringJUnit4ClassRunner.class)
// @SpringBootTest
// public class MongoServiceTest {

//     @Value("${app.mongodb.database}")
//     private String database;

//     @Value("${app.mongodb.collection.crud}")
//     private String crudCollection;
    
//     @Value("${app.mongodb.host}")
//     private String bindhost;

//     @Value("${app.mongodb.port}")
//     private int bindport;

//     @Autowired
//     private MongoService mongoService;



//     @BeforeClass
//     public static void beforeAll() throws Exception
//     {
//         String bindhost = "localhost";
//         int bindport = 12345;
//         MongodStarter starter = MongodStarter.getDefaultInstance();
//         IMongodConfig mongoConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
//             .net(new Net(bindhost,bindport,Network.localhostIsIPv6())).build();
//         MongodExecutable mongodx = starter.prepare(mongoConfig);
//         MongodProcess mongodp = mongodx.start();


//         System.out.println("Embeded Mongo Ready bro ...");
//     }

//     @Test
//     public void getDataCrudTest() throws Exception
//     {
//         MongoDatabase db =  new MongoClient(bindhost,bindport).getDatabase(database);
//         MongoCollection<Document> col = db.getCollection(crudCollection);

//         // String isi = new Scanner(new File("MongoJSONInsertMany.txt")).useDelimiter("\\A").next();
//         String isi = "{\"nama\":\"ulwicuk\",\"alamat\":\"rancaekekbergoyang\",\"pekerjaan\":\"ggwp\"}";
//         List<Document> value = new ArrayList<>();
//         value.add(Document.parse(isi));
//         col.insertMany(value);


//         Bson projection =  fields(Projections.excludeId());
//         List<Document> result =  mongoService.getDataCrud();


//         assertEquals("ulwicuk", result.get(0).getString("nama"));

//     }

//     @Test
//     public void insertDocumentTest() throws Exception
//     {
//         MongoDatabase db =  new MongoClient(bindhost,bindport).getDatabase(database);
//         MongoCollection<Document> col = db.getCollection(crudCollection);

//         String isi = "{\"nama\":\"ulwicuk\",\"alamat\":\"rancaekekbergoyang\",\"pekerjaan\":\"ggwp\"}";
//         Document parseisi = Document.parse(isi);
        
//         String insertResult = mongoService.insertDocument(parseisi);

//         assertEquals("inserted", insertResult);

//     }

//     @Test
//     public void updateDocumentTest() throws Exception
//     {
//         MongoDatabase db =  new MongoClient(bindhost,bindport).getDatabase(database);
//         MongoCollection<Document> col = db.getCollection(crudCollection);

//         String isi = "{\"nama\":\"idamganteng\",\"alamat\":\"rancaekek\",\"pekerjaan\":\"ggwp\"}";
//         List<Document> value = new ArrayList<>();
//         value.add(Document.parse(isi));
//         col.insertMany(value);


//         String isi2 = "{\"nama\":\"idamganteng\",\"alamat\":\"solo\",\"pekerjaan\":\"ggwp\"}";
//         Document parseisi2 = Document.parse(isi2);
        
//         String updateResult = mongoService.updateDocument("idamganteng",parseisi2);

//         assertEquals("updated", updateResult);

//     }

//     @Test
//     public void deleteDocumentTest() throws Exception
//     {
//         MongoDatabase db =  new MongoClient(bindhost,bindport).getDatabase(database);
//         MongoCollection<Document> col = db.getCollection(crudCollection);

//         String isi = "{\"nama\":\"idamganteng\",\"alamat\":\"rancaekek\",\"pekerjaan\":\"ggwp\"}";
//         List<Document> value = new ArrayList<>();
//         value.add(Document.parse(isi));
//         col.insertMany(value);

//         String deleteResult = mongoService.deleteDocument("idamganteng");

//         assertEquals("deleted", deleteResult);

//     }


// }
package com.example.crudservice.crudservice;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.example.crudservice.crudservice.controller.CrudController;
import com.example.crudservice.crudservice.service.CrudService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.reactive.server.WebTestClient;



@SuppressWarnings("MagicConstant")
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CrudServiceApplicationTests {

	@Autowired
	private WebTestClient webTestClient;

	@MockBean
	private CrudService crudService;

	@Autowired
	private CrudController CrudController;

	@Test
	public void contextLoads() {
	}


	@Test
	public void readTest(){
			this.webTestClient.get().uri("/read")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.exchange().expectStatus().
			.expectBody()
			.jsonPath("$.meta.message").isEqualTo("user tidak ditemukan");

		
	}



	

}

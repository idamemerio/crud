package com.example.crudservice.crudservice.response;


public class Response  {
    public Metadata meta;
    public Object data;
	
	public Response() {
		super();
	}

	public Response(Object object, String status, String message, Integer  code) {
		this.meta = new Metadata(status, message, code);
		this.data = object;
	}
	
	public Response(String status, String message, Integer  code) {
		this.meta = new Metadata(status, message, code);
	}
	
	public Response loaded(Object object)
	{
		Response tes = new Response(object, "success", "loaded successful bro", 200);

		System.out.println(tes.toString());
		return  new Response(object, "success", "loaded successful bro", 200);
	}
	
	public Response create()
	{
		return  new Response("success", "create successful bro", 201);
    }
    
    public Response update()
	{
		return  new Response("success", "update successful bro", 201);
	}

	public Response delete()
	{
		return  new Response("success", "delete successful bro", 201);
	}
	
	// public Response notFound(String message)
	// {
	// 	return new Response("failed", message, 404);
	// }
	
	// public Response badRequest(String message)
	// {
	// 	return new Response("failed", message, 400);
	// }

	// public Response unauthorized()
	// {
	// 	return new Response("failed", "Unauthorized", 401);
	// }

	public Response internalServerError(String message)
	{
		return new Response("failed",message, 500);
	}

	public Object getData()
	{
		return this.data;
	}
}

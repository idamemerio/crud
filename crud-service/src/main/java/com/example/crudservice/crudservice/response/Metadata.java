package com.example.crudservice.crudservice.response;

public class Metadata {
	
	public String status;
	public String message;
	public Integer code;
	
	public Metadata( String status, String message, Integer code) {
		this.status = status;
		this.message = message;
		this.code = code;
	}
}
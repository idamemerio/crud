package com.example.crudservice.crudservice.controller;


import java.util.List;

import javax.websocket.server.PathParam;

import com.example.crudservice.crudservice.response.Response;
import com.example.crudservice.crudservice.service.CrudService;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class CrudController{
    
    @Autowired
    private CrudService crudService;

    Response response = new Response();

    @RequestMapping(method = RequestMethod.POST, value = "/read", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response> read(@PathVariable String formcode) 
    {
        try
        {

            List<Document> output = crudService.readData();
            // System.out.println(output);
            return Mono.just(response.loaded(output));

        } catch (Exception e)
        {
            return Mono.just(response.internalServerError(e.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response> create(@RequestBody String payload) 
    {
        try
        {

            crudService.create(payload);
            // System.out.println(output);
            return Mono.just(response.create());

        } catch (Exception e)
        {
            return Mono.just(response.internalServerError(e.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/{nama}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response> update(@RequestBody String payload,@PathVariable String nama) 
    {
        try
        {

            crudService.update(nama,payload);
            // System.out.println(output);
            return Mono.just(response.update());

        } catch (Exception e)
        {
            return Mono.just(response.internalServerError(e.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{nama}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response> delete(@PathVariable String nama) 
    {
        try
        {

            crudService.delete(nama);
            // System.out.println(output);
            return Mono.just(response.delete());

        } catch (Exception e)
        {
            return Mono.just(response.internalServerError(e.toString()));
        }
    }

  
}
package com.example.crudservice.crudservice.service;


import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static com.mongodb.client.model.Filters.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class MongoService {

    @Value("${app.mongodb.database}")
    private String database;

    @Value("${app.mongodb.collection.crud}")
    private String crudCollection;
    
    @Value("${app.mongodb.host}")
    private String host;

    @Value("${app.mongodb.port}")
    private String port;

    private MongoClient getConnection()
    {
        return MongoClients.create("mongodb://"+this.host+":"+this.port);
    }

    public MongoDatabase getDatabase(String database)
    {
        return this.getConnection().getDatabase(database);
    }


    public List<Document> getDataCrud() 
    {
        
            MongoClient client = this.getConnection();
            try{
                MongoDatabase db = client.getDatabase(database);
                List<Document> output = new ArrayList<>();
                {
                    output = db.getCollection(crudCollection).find().projection(Projections.excludeId()).into(new ArrayList<>());
                }
                return output;
            } finally {
                client.close();
            }
    }

    public String insertDocument(Document newDocument)
    {
        MongoClient client = this.getConnection();
        try {
            MongoDatabase db = client.getDatabase(database);
            db.getCollection(crudCollection).insertOne(newDocument);
            return "inserted";
        } catch (Exception e) {
            return null;
        } finally
        {
            client.close();
        }
    }

    public String updateDocument(String nama, Document updatedDocument)
    {
        MongoClient client = this.getConnection();
        try{
            MongoDatabase db = client.getDatabase(database);
            Document dataupdate = new Document("$set",updatedDocument);
            Document output = db.getCollection(crudCollection).findOneAndUpdate(eq("nama", nama), dataupdate);
            System.out.println("oioioi"+output.toJson());
            return "updated";
        } finally {
            client.close();
        }
    }

    public String deleteDocument(String nama)
    {
        MongoClient client = this.getConnection();
        try{
            MongoDatabase db = client.getDatabase(database);
            Document output = db.getCollection(crudCollection).findOneAndDelete(Filters.eq("nama", nama));
            return "deleted";

        } finally {
            client.close();
        }
    }
}

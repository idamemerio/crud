package com.example.crudservice.crudservice.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrudService {
  
    @Autowired
    private MongoService mongoService;

    public List<Document> readData(){
        List<Document> output = new ArrayList<>();
        
   
        List<Document> data = mongoService.getDataCrud();

        List<Document> content = new ArrayList<>();
        for(Document doc : data )
        {
            Document out = new Document();
            out.append("nama", doc.getString("nama"));
            out.append("alamat", doc.getString("alamat"));
            out.append("pekerjaan", doc.getString("pekerjaan"));
            content.add(out);

        }

        output.add(new Document().append("content",content));
        System.out.println("oioioi:  "+output.toString());

        

        return output;
    }

    public String create(String payload)
    {
        Document payloadData = Document.parse(payload);
        String output = this.mongoService.insertDocument(payloadData);
        return output;
    }

    public String update(String nama ,String payload)
    {
        Document payloadData = Document.parse(payload);
        String output = this.mongoService.updateDocument(nama,payloadData);
        return output;
    }

    public String delete(String nama)
    {
        String output = this.mongoService.deleteDocument(nama);
        return output;
    }
}